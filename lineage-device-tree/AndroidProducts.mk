#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_X01BD.mk

COMMON_LUNCH_CHOICES := \
    lineage_X01BD-user \
    lineage_X01BD-userdebug \
    lineage_X01BD-eng
